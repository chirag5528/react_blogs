import React from 'react'
import Header from '../components/Header.jsx'

import Subscribe from '../components/Subscribe.jsx'
import Footer from '../components/Footer.jsx'
import Navbar from '../components/Navbar.jsx'
const BlogLayout = ({children}) => {
    return (
        <React.Fragment>
            <Header/>
            <div className="container px-4 md:px-0 max-w-6xl mx-auto -mt-32 w-full">
                <div className="mx-0 sm:mx-6">
                    <Navbar />
                    <div className="bg-gray-200 w-full text-xl md:text-2xl text-gray-800 leading-normal rounded-t">
                        {children}  
                        <Subscribe/>
                    </div>
                </div>
            </div>
            <Footer />
        </React.Fragment>
    )
}

export default BlogLayout