import React, {Fragment} from 'react'
import Home from './Pages/Home.jsx'
import Blogs from './Pages/Blogs.jsx'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
const App = () => {
  return (
    <Fragment>
      <Router>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/blogs" component={Blogs} />
        </Switch>
      </Router>
      
    </Fragment>
  )
}
export default App
