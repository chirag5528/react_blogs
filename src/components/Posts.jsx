import React from 'react'

export default function Posts({thumbnail,thumbnailDescription,text,title,authorTitle,authorImage,authorImageDescription,rowClass,readTime}) {

    return (
        <React.Fragment>
            <div className={`w-full p-6 flex flex-col flex-grow flex-shrink ${rowClass}`}>
                <div className="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow-lg">
                    <button className="flex flex-wrap no-underline hover:no-underline">
                        <img src={thumbnail} alt={thumbnailDescription} className="h-64 w-full rounded-t pb-6" />
                        <p className="w-full text-gray-600 text-xs md:text-sm px-6">GETTING STARTED</p>
                        <div className="w-full font-bold text-xl text-gray-900 px-6">{title}</div>
                        <p className="text-gray-800 font-serif text-base px-6 mb-5">
                            {text}
                        </p>
                    </button>
                </div>
                <div className="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow-lg p-6">
                    <div className="flex items-center justify-between">
                        <img className="w-8 h-8 rounded-full mr-4 avatar" data-tippy-content={authorTitle} src={authorImage} alt={authorImageDescription} />
                        <p className="text-gray-600 text-xs md:text-sm">{readTime} MIN READ</p>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}
