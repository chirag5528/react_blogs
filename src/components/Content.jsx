import React, {useState,useEffect} from 'react'
import TwoPostsRow from './TwoPostsRow.jsx'
import client from '../client'
const Content = () =>{

    const [posts,setPosts] = useState([])
    
    const  fetchData = (client) => {
        client.getEntries({
            'content_type': 'blogPost'
        })
        .then((res) => {
            setPosts(res)
        })
        .catch((err) => console.log(err))
    }

    useEffect( () => {
        fetchData(client)
    },[])

    return (
        <React.Fragment>
            <div className="flex flex-wrap justify-between pt-12 -mx-6">
                <TwoPostsRow posts={posts} />
            </div>
        </React.Fragment>
    )
}

export default Content