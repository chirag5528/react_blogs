import React from 'react'
import logo from '../images/content-bloom-logo.png'
import {Link} from 'react-router-dom'
const Footer = () => {
    return(
        <React.Fragment>
            <footer className="bg-gray-900">	
                <div className="container max-w-6xl mx-auto flex items-center px-2 py-8">
                    <div className="w-full mx-auto flex flex-wrap items-center">
                        <div className="flex w-full md:w-1/2 justify-center md:justify-start text-white font-extrabold">
                            <button className="text-gray-900 no-underline hover:text-gray-900 hover:no-underline" >
                                <img alt="content-bloom-logo" src={logo} className="max-w-sm w-48 inline" />
                            </button>
                        </div>
                        <div className="flex w-full pt-2 content-center justify-between md:w-1/2 md:justify-end">
                            <ul className="list-reset flex justify-center flex-1 md:flex-none items-center">
                                <li>
                                    <Link to="/" className="inline-block py-2 px-3 text-white no-underline">Home</Link>
                                </li>
                                <li>
                                    <Link to="/blogs" className="inline-block text-gray-600 no-underline hover:text-gray-200 hove
                                    r:underline py-2 px-3">Blogs</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        </React.Fragment>
    )
}

export default Footer