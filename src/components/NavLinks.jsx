import React from 'react'
import {Link} from 'react-router-dom'
export default function NavLinks() {
    return (
        <React.Fragment>
            <ul className="list-reset flex justify-between flex-1 md:flex-none items-center">
                <li className="mr-2">
                    <Link to="/" className="inline-block py-2 px-2 text-white no-underline hover:underline">Home</Link>
                </li>
                <li className="mr-2">
                    <Link to="/blogs" className="inline-block text-gray-600 no-underline hover:text-gray-200 hover:underline py-2 px-2">Blogs</Link>
                </li>
            </ul>
        </React.Fragment>
    )
}
