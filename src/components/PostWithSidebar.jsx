import React from 'react'
import Posts from './Posts.jsx'
const PostWithSidebar = () => {
    return (
        <React.Fragment>
            <Posts 
                thumbnail="https://source.unsplash.com/collection/539527/800x600"
                title="Lorem ipsum dolor sit amet."
                text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. "
                userImage="http://i.pravatar.cc/300"
                readTime="1"
            />
        </React.Fragment>
    )
}
export default PostWithSidebar