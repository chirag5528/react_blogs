const postsData = [

    {
        thumbnail: "https://source.unsplash.com/collection/225/800x600",
        title: "Lorem ipsum dolor sit amet.",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. ",
        userImage: "http://i.pravatar.cc/300",
        readTime: "1",
    },
    {
        thumbnail: "https://source.unsplash.com/collection/3106804/800x600",
        title: "Lorem ipsum dolor sit amet.",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. ",
        userImage: "http://i.pravatar.cc/300",
        readTime: "1",
    },
    {
        thumbnail: "https://source.unsplash.com/collection/3657445/800x600",
        title: "Lorem ipsum dolor sit amet.",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. ",
        userImage: "http://i.pravatar.cc/300",
        readTime: "1",
    },
    {
        thumbnail: "https://source.unsplash.com/collection/764827/800x600",
        title: "Lorem ipsum dolor sit amet.",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. ",
        userImage: "http://i.pravatar.cc/300",
        readTime: "1",
    },
    {
        thumbnail: "https://source.unsplash.com/collection/325867/800x600",
        title: "Lorem ipsum dolor sit amet.",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. ",
        userImage: "http://i.pravatar.cc/300",
        readTime: "1",
    },
]
export default postsData