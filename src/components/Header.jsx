import React from 'react'
import cover from '../images/cover.jpg'
import logo from "../images/content-bloom-logo.png"
const Header = () =>{
    const headerStyle = {
        backgroundImage: `url(${cover})`,
        height: "60vh",
        maxHeight: "460px"
    }
    return(
        <React.Fragment>
            <div className="w-full m-0 p-0 bg-cover bg-bottom" style={headerStyle}>
                <div className="container max-w-4xl mx-auto pt-16 md:pt-32 text-center break-normal">
                    <p className="text-white font-extrabold">
                        <img alt="content-bloom-logo" src={logo} className="max-w-sm inline" />
                    </p>
                    <p className="text-xl md:text-2xl text-gray-500">Welcome to CB Blogs</p>
                </div>
	        </div>
        </React.Fragment>
    )
}
export default Header