import React from 'react'
import NavLinks from './NavLinks.jsx'
import SocialLinks from './SocialLinks.jsx'
import NavbarBanner from './NavbarBanner.jsx'
export const Navbar = () => {
    return (
        <React.Fragment>
            <nav className="mt-0 w-full">
                <div className="container mx-auto flex items-center">
                    <div className="flex w-1/2 pl-4 text-sm">
                        <NavLinks />
                    </div>
                    <SocialLinks />
                </div>
            </nav>
            <NavbarBanner />
        </React.Fragment>
    )
}
export default Navbar
