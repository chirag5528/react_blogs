import React from 'react'
import Posts from './Posts.jsx'
const ThreePostsRow = ({posts}) => {
    return (
        <React.Fragment>
            {
                posts.map( ({thumbnail,title,text, userImage,readTime},index) => {
                    if(index >= 3){
                        return (
                            <Posts 
                                rowClass="md:w-1/2"
                                thumbnail={thumbnail}
                                title={title}
                                text={text}
                                userImage={userImage}
                                readTime={readTime}
                                key={index}
                            />
                        )
                    }else{
                        return ""
                    }
                })
            }
        </React.Fragment>
    )
}

export default ThreePostsRow