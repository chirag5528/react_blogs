import React from 'react'
import Posts from './Posts.jsx'
const TwoPostsRow = ({posts:{items}}) => {
    
    React.useEffect( () => {},[items])

    return (
        <React.Fragment>
            { items &&
               items.map(({fields,index}) => {
                    return(
                        <Posts 
                            rowClass="md:w-1/3"
                            readTime="1"
                            thumbnail={fields.heroImage.fields.file.url}
                            thumbnailDescription={fields.heroImage.fields.file.description}
                            text={fields.description}
                            title={fields.title}
                            authorTitle={fields.author.fields.name}
                            authorImage={fields.author.fields.image.fields.file.url}
                            authorImageDescription={fields.author.fields.image.fields.description}
                            key={index}
                        />
                    )
                })
            }
        </React.Fragment>
    )
}

export default TwoPostsRow