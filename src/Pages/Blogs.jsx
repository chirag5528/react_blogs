import React from 'react'
import HomeLayout from '../Layouts/HomeLayout.jsx'
import Content from '../components/Content.jsx'
const Blogs = () => {
    return (
        <React.Fragment>
            <HomeLayout>
                <Content />
            </HomeLayout>
        </React.Fragment>
    )
}

export default Blogs