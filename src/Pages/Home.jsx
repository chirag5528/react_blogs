import React from 'react'
// import Content from '../components/Content.jsx'
import Posts from '../components/Posts.jsx'
import HomeLayout from '../Layouts/HomeLayout.jsx'
const Home = () => {

    const twoPostsRow = [
        {
            rowClass:"md:w-1/3",
            readTime:"1",
            thumbnail:"https://source.unsplash.com/collection/225/800x600",
            thumbnailDescription:"Lorem Image",
            text:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. ",
            title:"Lorem ipsum dolor sit amet.",
            authorTitle:"Chirag Arora",
            authorImage:"http://i.pravatar.cc/300",
            authorImageDescription:"Author Image",
        },
        {
            rowClass:"md:w-1/3",
            readTime:"1",
            thumbnail:"https://source.unsplash.com/collection/3106804/800x600",
            thumbnailDescription:"Lorem Image",
            text:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. ",
            title:"Lorem ipsum dolor sit amet.",
            authorTitle:"Chirag Arora",
            authorImage:"http://i.pravatar.cc/300",
            authorImageDescription:"Author Image",
        },
        {
            rowClass:"md:w-1/3",
            readTime:"1",
            thumbnail:"https://source.unsplash.com/collection/539527/800x600",
            thumbnailDescription:"Lorem Image",
            text:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. ",
            title:"Lorem ipsum dolor sit amet.",
            authorTitle:"Chirag Arora",
            authorImage:"http://i.pravatar.cc/300",
            authorImageDescription:"Author Image",
        },
         {
            rowClass:"md:w-1/3",
            readTime:"1",
            thumbnail:"https://source.unsplash.com/collection/3657445/800x600",
            thumbnailDescription:"Lorem Image",
            text:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. ",
            title:"Lorem ipsum dolor sit amet.",
            authorTitle:"Chirag Arora",
            authorImage:"http://i.pravatar.cc/300",
            authorImageDescription:"Author Image",
        },
        {
            rowClass:"md:w-1/3",
            readTime:"1",
            thumbnail:"https://source.unsplash.com/collection/325867/800x600",
            thumbnailDescription:"Lorem Image",
            text:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. ",
            title:"Lorem ipsum dolor sit amet.",
            authorTitle:"Chirag Arora",
            authorImage:"http://i.pravatar.cc/300",
            authorImageDescription:"Author Image",
        },
         {
            rowClass:"md:w-2/3",
            readTime:"1",
            thumbnail:"https://source.unsplash.com/collection/325867/800x600",
            thumbnailDescription:"Lorem Image",
            text:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. ",
            title:"Lorem ipsum dolor sit amet.",
            authorTitle:"Chirag Arora",
            authorImage:"http://i.pravatar.cc/300",
            authorImageDescription:"Author Image",
        },
        {
            rowClass:"md:w-1/3",
            readTime:"1",
            thumbnail:"https://source.unsplash.com/collection/1118905/800x600",
            thumbnailDescription:"Lorem Image",
            text:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula. ",
            title:"Lorem ipsum dolor sit amet.",
            authorTitle:"Chirag Arora",
            authorImage:"http://i.pravatar.cc/300",
            authorImageDescription:"Author Image",
        }
    ]

    return (
        <React.Fragment>
            <HomeLayout>
                <div className="flex flex-wrap justify-between pt-12 -mx-6">
                    {
                        twoPostsRow.map((post) => {
                            return (
                                <Posts 
                                    rowClass={post.rowClass}
                                    readTime={post.readTime}
                                    thumbnail={post.thumbnail}
                                    thumbnailDescription={post.thumbnailDescription}
                                    text={post.text}
                                    title={post.title}
                                    authorTitle={post.authorTitle}
                                    authorImage={post.authorImage}
                                    authorImageDescription={post.authorImageDescription}
                                    key={post.index}
                                />
                            )
                        })
                    }
                </div>
            </HomeLayout>
        </React.Fragment>
    )
}
export default Home